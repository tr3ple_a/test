<!DOCTYPE html>
<html lang="en">
<head>
  <title>Interview Test</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>User List</h2>      
  <table class="table">
    <thead>
	  <tr>
        <th>Current User : {{ $curr_user->name }}</th>
        <th class="text-right">
			<form action="{{ route('users.logout') }}" method="post">
			@csrf
			<input type="hidden" name="token" value="{{ $token }}">
			<button type="submit">Logout</button>
			</form>
		</th>
      </tr>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
	  @foreach($users as $user)
      <tr>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td><a href="{{ route('users.edit',$user->id) }}?token={{ $token }}">Edit</a></td>
        <td>
			<form action="{{ route('users.delete',$user->id) }}" method="post">
			@csrf
			<input type="hidden" name="token" value="{{ $token }}">
			<button type="submit">Delete</button>
			</form>
		</td>
      </tr>
	  @endforeach
    </tbody>
  </table>
</div>

</body>
</html>
