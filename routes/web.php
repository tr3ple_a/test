<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
		Route::get('/register', ['as'=>'register','uses'=>'App\Http\Controllers\UserController@register']);
		Route::post('/store', ['as'=>'store','uses'=>'App\Http\Controllers\UserController@store']);
		Route::post('/login', ['as'=>'login','uses'=>'App\Http\Controllers\UserController@login']);
		Route::get('/list', ['as'=>'list','uses'=>'App\Http\Controllers\UserController@list']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'App\Http\Controllers\UserController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'App\Http\Controllers\UserController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'App\Http\Controllers\UserController@delete']);
		Route::post('/logout', ['as'=>'logout','uses'=>'App\Http\Controllers\UserController@logout']);
	});
