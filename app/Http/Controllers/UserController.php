<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Models\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Session;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['login','register','store']]);
    }
	
	public function register() 
	{
		return view('register');
	}
	
	public function store(Request $request)
    {
    	//Validate data
        $data = $request->only('name', 'email', 'password');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, create new user
        $user = User::create([
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => bcrypt($request->password)
        ]);
		
		$credentials = $request->only('email', 'password');
		
        //User created, return success response
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }
		
		Session::put('token', $token);
		return redirect('users/list?token=' . $token);
    }
	
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated
        //Crean token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }
		
		Session::put('token', $token);
		return redirect('users/list?token=' . $token);
		
    }
	
	public function list(Request $request) 
	{
		$token = Session::get('token');
		$curr_user = JWTAuth::authenticate($request->token);
		if($curr_user)
		{
			$users= User::where('id','<>',$curr_user->id)->get();
			return view('users', compact('users','token','curr_user'));
		}
		return 'Error';
	}
	
	public function edit(Request $request, $id) 
	{
		$token = Session::get('token');
		$curr_user = JWTAuth::authenticate($request->token);
		if($curr_user)
		{
			$user= User::find($id);
			return view('edit', compact('user','token'));
		}
		return 'Error';
	}
	
	public function update(Request $request, $id) 
	{
        $curr_user = JWTAuth::authenticate($request->token);
		if($curr_user)
		{
			$user= User::find($id);
			$user->email = $request->email;
			$user->name = $request->name;
			$user->save();
			return redirect()->back();
		}
		return 'Error';
	}
	
	public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

		//Request is validated, do logout        
        try {
            JWTAuth::invalidate($request->token);
 
            return redirect('/');
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
	
	public function delete(Request $request, $id) 
	{
		$token = Session::get('token');
		$curr_user = JWTAuth::authenticate($request->token);
		if($curr_user)
		{
			$user= User::where('id',$id)->delete();
			return redirect('users/list?token=' . $token);
		}
		return 'Error';
	}
}
